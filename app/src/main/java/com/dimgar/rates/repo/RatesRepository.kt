package com.dimgar.rates.repo

import com.dimgar.rates.api.RatesApi
import com.dimgar.rates.api.RatesResponse.Companion.toListOfCurrencies
import com.dimgar.rates.core.CurrencyEnum
import com.dimgar.rates.core.ViewModelState
import com.dimgar.rates.db.currency.CurrencyDao
import com.dimgar.rates.db.currency.CurrencyEntity
import com.dimgar.rates.db.currency.toRateItemViewModels
import com.dimgar.rates.ui.adapter.viewmodel.RateItemViewModels
import dagger.Module
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject


@Module
class RatesRepository @Inject constructor(
    private val api: RatesApi,
    private val currencyDao: CurrencyDao
) {

    fun getCurrencies(base: CurrencyEnum?): Flowable<RateItemViewModels> {
        return Flowable.merge(
            getCurrenciesFromDb(ViewModelState.LOCAL)
                .toFlowable(BackpressureStrategy.LATEST),
            getCurrenciesFromApi(base)
        )
    }

    private fun getCurrenciesFromDb(state: ViewModelState): Observable<RateItemViewModels> {

        return currencyDao.getAll()
            .onErrorResumeNext {
                Single.just(listOf())
            }
            .map {
                it.toRateItemViewModels(state)
            }
            .toObservable()
    }

    private fun getCurrenciesFromApi(base: CurrencyEnum?): Flowable<RateItemViewModels> {
        return Observable.interval(0, 1, TimeUnit.SECONDS)
            .flatMap {
                // base will be null each time the app is started, so we need to ask the db, what was
                // the last Currency that was fetched
                // if no currency is found to be base, we proceed with requesting for EUR
                if (base == null) {
                    currencyDao.getBase()
                        .onErrorReturn {
                            CurrencyEnum.EUR
                        }
                        .toObservable()
                } else {
                    Observable.just(base)
                }
            }
            .flatMap {
                api.getRates(it)
            }
            .flatMap {
                storeCurrenciesInDb(it.toListOfCurrencies()).toObservable()
            }
            .flatMap {
                getCurrenciesFromDb(ViewModelState.UPDATED)
            }
            .onErrorResumeNext { e: Throwable ->

                Timber.e(e)

                getCurrenciesFromDb(ViewModelState.ERROR)
            }
            .toFlowable(BackpressureStrategy.LATEST)

    }

    private fun storeCurrenciesInDb(currencies: List<CurrencyEntity>): Single<List<Long>> {
        return currencyDao.insertAll(currencies)
    }
}