package com.dimgar.rates.di

import android.content.Context
import androidx.room.Room
import com.dimgar.rates.api.RatesApi
import com.dimgar.rates.db.RatesDatabase
import com.dimgar.rates.repo.RatesRepository
import com.dimgar.rates.db.currency.CurrencyDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object RepositoryModule {
    @Provides
    @Singleton
    @JvmStatic
    fun provideRatesDatabase(context: Context): RatesDatabase {
        return Room.databaseBuilder(context.applicationContext, RatesDatabase::class.java, "rates_database")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    @JvmStatic
    fun provideCurrencyDao(database: RatesDatabase): CurrencyDao {
        return database.currencyDao()
    }

    @Provides
    @Singleton
    @JvmStatic
    fun provideRatesRepository(api: RatesApi, currencyDao: CurrencyDao): RatesRepository {
        return RatesRepository(api, currencyDao)
    }
}