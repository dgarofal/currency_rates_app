package com.dimgar.rates.di

import android.app.Application
import com.dimgar.rates.RatesApplication
import com.dimgar.rates.di.activity.ActivityBuilder
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton


@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    AppModule::class,
    MoshiModule::class,
    NetworkModule::class,
    RepositoryModule::class,
    ActivityBuilder::class
])
interface AppComponent : AndroidInjector<RatesApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun application(application: Application)
}