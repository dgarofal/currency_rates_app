package com.dimgar.rates.di

import com.squareup.moshi.FromJson
import com.squareup.moshi.Moshi
import com.squareup.moshi.ToJson
import dagger.Module
import dagger.Provides
import java.math.BigDecimal
import javax.inject.Singleton

@Module
object MoshiModule {
    @Provides
    @Singleton
    @JvmStatic
    fun providesMoshi(): Moshi {
        return Moshi.Builder()
            .add(BigDecimalAdapter)
            .build()
    }
}

object BigDecimalAdapter {
    @FromJson
    fun fromJson(string: String) = BigDecimal(string)

    @ToJson
    fun toJson(value: BigDecimal) = value.toString()
}
