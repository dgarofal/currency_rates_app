package com.dimgar.rates.api
import com.dimgar.rates.api.RatesItemsResponse
import com.dimgar.rates.api.RatesResponse.Companion.CURRENCY_RATE_ONE
import com.dimgar.rates.core.CurrencyEnum
import com.dimgar.rates.db.currency.CurrencyEntity
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RatesResponse(
    @field:Json(name = "base")
    val baseCurrency: CurrencyEnum,

    @field:Json(name = "rates")
    val ratesItemsResponse: RatesItemsResponse
) {
    companion object {
        const val CURRENCY_RATE_ONE = "1"

        // TODO: maybe vararg Enums here?
        fun RatesResponse.toListOfCurrencies(): List<CurrencyEntity> {
            return ratesItemsResponse.run {
                listOf(
                    CurrencyEntity(currencyEnum = CurrencyEnum.AUD, currencyRate = AUD ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.BGN, currencyRate = BGN ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.BRL, currencyRate = BRL ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.CAD, currencyRate = CAD ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.CHF, currencyRate = CHF ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.CNY, currencyRate = CNY ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.CZK, currencyRate = CZK ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.DKK, currencyRate = DKK ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.EUR, currencyRate = EUR ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.GBP, currencyRate = GBP ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.HKD, currencyRate = HKD ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.HRK, currencyRate = HRK ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.HUF, currencyRate = HUF ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.IDR, currencyRate = IDR ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.ILS, currencyRate = ILS ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.INR, currencyRate = INR ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.ISK, currencyRate = ISK ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.JPY, currencyRate = JPY ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.KRW, currencyRate = KRW ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.MXN, currencyRate = MXN ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.MYR, currencyRate = MYR ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.NOK, currencyRate = NOK ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.NZD, currencyRate = NZD ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.PHP, currencyRate = PHP ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.PLN, currencyRate = PLN ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.RON, currencyRate = RON ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.RUB, currencyRate = RUB ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.SEK, currencyRate = SEK ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.SGD, currencyRate = SGD ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.THB, currencyRate = THB ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.TRY, currencyRate = TRY ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.USD, currencyRate = USD ?: CURRENCY_RATE_ONE),

                    CurrencyEntity(currencyEnum = CurrencyEnum.ZAR, currencyRate = ZAR ?: CURRENCY_RATE_ONE)
                )
            }
        }
    }
}