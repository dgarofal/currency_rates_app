package com.dimgar.rates.api

import com.dimgar.rates.core.CurrencyEnum
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface RatesApi {
    @GET("latest")
    fun getRates(@Query("base") baseCurrency: CurrencyEnum): Observable<RatesResponse>
}