package com.dimgar.rates.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

// TODO: maybe vararg Enums here?
@JsonClass(generateAdapter = true)
data class RatesItemsResponse(
    @field: Json(name="AUD")
    val AUD: String? = null,
    @field: Json(name="BGN")
    val BGN: String? = null,
    @field: Json(name="BRL")
    val BRL: String? = null,
    @field: Json(name="CAD")
    val CAD: String? = null,
    @field: Json(name="CHF")
    val CHF: String? = null,
    @field: Json(name="CNY")
    val CNY: String? = null,
    @field: Json(name="CZK")
    val CZK: String? = null,
    @field: Json(name="DKK")
    val DKK: String? = null,
    @field: Json(name="EUR")
    val EUR: String? = null,
    @field: Json(name="GBP")
    val GBP: String? = null,
    @field: Json(name="HKD")
    val HKD: String? = null,
    @field: Json(name="HRK")
    val HRK: String? = null,
    @field: Json(name="HUF")
    val HUF: String? = null,
    @field: Json(name="IDR")
    val IDR: String? = null,
    @field: Json(name="ILS")
    val ILS: String? = null,
    @field: Json(name="INR")
    val INR: String? = null,
    @field: Json(name="ISK")
    val ISK: String? = null,
    @field: Json(name="JPY")
    val JPY: String? = null,
    @field: Json(name="KRW")
    val KRW: String? = null,
    @field: Json(name="MXN")
    val MXN: String? = null,
    @field: Json(name="MYR")
    val MYR: String? = null,
    @field: Json(name="NOK")
    val NOK: String? = null,
    @field: Json(name="NZD")
    val NZD: String? = null,
    @field: Json(name="PHP")
    val PHP: String? = null,
    @field: Json(name="PLN")
    val PLN: String? = null,
    @field: Json(name="RON")
    val RON: String? = null,
    @field: Json(name="RUB")
    val RUB: String? = null,
    @field: Json(name="SEK")
    val SEK: String? = null,
    @field: Json(name="SGD")
    val SGD: String? = null,
    @field: Json(name="THB")
    val THB: String? = null,
    @field: Json(name="TRY")
    val TRY: String? = null,
    @field: Json(name="USD")
    val USD: String? = null,
    @field: Json(name="ZAR")
    val ZAR: String? = null
)