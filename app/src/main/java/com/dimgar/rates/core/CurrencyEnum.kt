package com.dimgar.rates.core

import androidx.room.TypeConverter
import com.dimgar.rates.R

enum class CurrencyEnum(val displayName: Int, val drawable: Int) {
    AUD(R.string.currency_name_AUD, R.drawable.ic_australia),
    BGN(R.string.currency_name_BGN, R.drawable.ic_bulgaria),
    BRL(R.string.currency_name_BRL, R.drawable.ic_brazil),
    CAD(R.string.currency_name_CAD, R.drawable.ic_canada),
    CHF(R.string.currency_name_CHF, R.drawable.ic_switzerland),
    CNY(R.string.currency_name_CNY, R.drawable.ic_china),
    CZK(R.string.currency_name_CZK, R.drawable.ic_czech_republic),
    DKK(R.string.currency_name_DKK, R.drawable.ic_denmark),
    EUR(R.string.currency_name_EUR, R.drawable.ic_european_union),
    GBP(R.string.currency_name_GBP, R.drawable.ic_england),
    HKD(R.string.currency_name_HKD, R.drawable.ic_hong_kong),
    HRK(R.string.currency_name_HRK, R.drawable.ic_croatia),
    HUF(R.string.currency_name_HUF, R.drawable.ic_hungary),
    IDR(R.string.currency_name_IDR, R.drawable.ic_indonesia),
    ILS(R.string.currency_name_ILS, R.drawable.ic_israel),
    INR(R.string.currency_name_INR, R.drawable.ic_india),
    ISK(R.string.currency_name_ISK, R.drawable.ic_iceland),
    JPY(R.string.currency_name_JPY, R.drawable.ic_japan),
    KRW(R.string.currency_name_KRW, R.drawable.ic_south_korea),
    MXN(R.string.currency_name_MXN, R.drawable.ic_mexico),
    MYR(R.string.currency_name_MYR, R.drawable.ic_malaysia),
    NOK(R.string.currency_name_NOK, R.drawable.ic_norway),
    NZD(R.string.currency_name_NZD, R.drawable.ic_new_zealand),
    PHP(R.string.currency_name_PHP, R.drawable.ic_philippines),
    PLN(R.string.currency_name_PLN, R.drawable.ic_poland),
    RON(R.string.currency_name_RON, R.drawable.ic_romania),
    RUB(R.string.currency_name_RUB, R.drawable.ic_russia),
    SEK(R.string.currency_name_SEK, R.drawable.ic_sweden),
    SGD(R.string.currency_name_SGD, R.drawable.ic_singapore),
    THB(R.string.currency_name_THB, R.drawable.ic_thailand),
    TRY(R.string.currency_name_TRY, R.drawable.ic_turkey),
    USD(R.string.currency_name_USD, R.drawable.ic_united_states_of_america),
    ZAR(R.string.currency_name_ZAR, R.drawable.ic_south_africa);
}