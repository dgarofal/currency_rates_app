package com.dimgar.rates.core.bindings

import android.content.Context
import androidx.appcompat.widget.AppCompatEditText
import androidx.databinding.BindingAdapter
import android.view.inputmethod.InputMethodManager.SHOW_IMPLICIT
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat.getSystemService



@BindingAdapter("requestFocus")
fun AppCompatEditText.requestFocus(focus: Boolean) {
    if (focus == hasFocus()) {
        return
    }

    if (focus) {
        requestFocus()

        val imm = context.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.showSoftInput(this, SHOW_IMPLICIT)
    } else {
        clearFocus()
    }
}