package com.dimgar.rates.core.bindings

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import timber.log.Timber
import androidx.recyclerview.widget.SimpleItemAnimator
import android.content.Context.INPUT_METHOD_SERVICE
import androidx.core.content.ContextCompat.getSystemService
import android.view.MotionEvent
import android.view.View.OnTouchListener
import android.view.inputmethod.InputMethodManager

@BindingAdapter("hasFixedSize")
fun RecyclerView.mSetHasFixedSize(hasFixedSize: Boolean) {
    this.setHasFixedSize(hasFixedSize)
}

@BindingAdapter("hasItemAnimations")
fun RecyclerView.setHasItemAnimations(hasItemAnimations: Boolean) {
    (itemAnimator as? SimpleItemAnimator?)?.supportsChangeAnimations = hasItemAnimations
}