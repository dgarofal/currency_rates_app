package com.dimgar.rates.core

enum class ViewModelState {LOADING, LOCAL, UPDATED, ERROR}