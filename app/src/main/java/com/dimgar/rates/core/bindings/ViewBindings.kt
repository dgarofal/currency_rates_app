package com.dimgar.rates.core.bindings

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.databinding.BindingAdapter

@BindingAdapter("hideKeyboardOnTouch")
fun View.hideKeyboardOnTouch(hide: Boolean) {
    if (!hide) {
        return
    }

    setOnTouchListener { v, _ ->
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.hideSoftInputFromWindow(v.windowToken, 0)

        false
    }
}

@BindingAdapter("booleanVisibile")
fun View.booleanVisibile(visible: Boolean) {
    if (visible) {
        visibility = View.VISIBLE
    } else {
        visibility = View.GONE
    }

}