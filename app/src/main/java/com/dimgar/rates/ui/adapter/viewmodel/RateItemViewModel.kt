package com.dimgar.rates.ui.adapter.viewmodel

import android.view.View
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import com.dimgar.rates.api.RatesItemsResponse
import com.dimgar.rates.core.CurrencyEnum
import com.dimgar.rates.core.ViewModelState
import timber.log.Timber
import java.lang.ref.WeakReference
import java.math.BigDecimal
import java.math.RoundingMode


class RateItemViewModels(var state: ViewModelState, var items: List<RateItemViewModel>)

class RateItemViewModel(
    val currencyEnum: CurrencyEnum,
    var apiCurrencyRate: String,
    var apiCurrencyRateBigDecimal: BigDecimal,
    var isBase: Boolean
) : BaseObservable(), View.OnFocusChangeListener {

    companion object {
        fun List<RateItemViewModel>.getBase() = this.find { it.isBase }
        fun List<RateItemViewModel>.getFocused() = this.find { it.hasFocus }

        // This computation takes place because we may still not have the focused item's rates from the api
        // (e.g in case of network error and the user has switched currency)
        // so for these cases, dividing focused item's value by focused item's api rate and multiplying with the base we got,
        // gives us our new base.
        // In cases where the api is updating OK, the base will compute as scaledFocusedItemTextValue * base,
        // because focusedItemApiRate should be 1
        fun computeNewRate(
            focusedItemTextValue: String?,
            focusedItemApiRate: BigDecimal,
            baseApiRate: BigDecimal
        ): BigDecimal {


            val mFocusedItemTextValue: BigDecimal
            try {
                mFocusedItemTextValue = BigDecimal(focusedItemTextValue)
            } catch (e: Exception) {
                return BigDecimal.ZERO
            }

            return try {
                val scaledFocusedItemTextValue =
                    mFocusedItemTextValue.setScale(20, RoundingMode.HALF_EVEN)

                (scaledFocusedItemTextValue / focusedItemApiRate) * baseApiRate

            } catch (e: Exception) {
                BigDecimal.ZERO
            }
        }

        fun List<RateItemViewModel>?.update(newItems: List<RateItemViewModel>): MutableList<RateItemViewModel> {
            if (this.isNullOrEmpty()) {
                return newItems.toMutableList()
            }

            // get focused item to use its current text value
            val focusedItem = getFocused()
            // get the new item that is the same as focused, to use the newest api rates
            val newFocusedItem = newItems.find { newItem -> newItem == focusedItem }
            // get the new base rate for conversions
            val baseItem = newItems.getBase()

            val rate = computeNewRate(
                focusedItem?.currencyRateText,
                newFocusedItem?.apiCurrencyRateBigDecimal ?: return newItems.toMutableList(),
                baseItem?.apiCurrencyRateBigDecimal ?: return newItems.toMutableList()
            )

            forEach { oldItem ->
                val newItem = newItems.find { it == oldItem }
                newItem ?: return@forEach

                oldItem.update(newItem, rate)
            }

            return this.toMutableList()
        }

        fun RateItemViewModel.update(
            newItem: RateItemViewModel,
            rate: BigDecimal
        ) {
            apiCurrencyRate = newItem.apiCurrencyRate
            isBase = newItem.isBase

            if (!hasFocus) {
                try {
                    currencyRateText =
                        if (rate.compareTo(BigDecimal.ZERO) == 0) "0" else (BigDecimal(this.apiCurrencyRate) * rate).setScale(
                            10,
                            RoundingMode.HALF_EVEN
                        ).stripTrailingZeros().toPlainString()
                } catch (e: Exception) {
                    Timber.d(e)
                }
            }
          }
    }

    private var gainedFocusListener: WeakReference<RateItemViewModelListenerOnGainedFocus>? = null
    private var textChangeListener: WeakReference<RateItemViewModelListenerOnTextChanged>? = null

    val currencyAbbreviationDisplayValue = currencyEnum.name
    val currencyDisplayName = currencyEnum.displayName
    val currencyFlag = currencyEnum.drawable

    var currencyRateText: String = apiCurrencyRate
        set(value) {
            // Avoid notifying when value has not changed
            if (field == value) {
                return
            }

            field = value
            notifyPropertyChanged(BR.currencyRateText)

            if (hasFocus) {
                textChangeListener?.get()?.onTextChanged(value)
            }
        }
        @Bindable get

    var hasFocus: Boolean = isBase
        set(value) {
            field = value
            notifyPropertyChanged(BR.hasFocus)
        }
        @Bindable get

    fun onClick() = changeFocus(true)

    override fun onFocusChange(v: View?, hasFocus: Boolean) = changeFocus(hasFocus)

    private fun changeFocus(hasFocus: Boolean) {
        // We only care if the item got focused
        if (hasFocus) {
            // Avoid notifying when value has not changed
            if (this.hasFocus == hasFocus) {
                return
            }

            this.hasFocus = hasFocus
            gainedFocusListener?.get()?.onGainedFocus(this)
        }
    }

    fun setGainedFocusListener(listener: WeakReference<RateItemViewModelListenerOnGainedFocus>?) {
        this.gainedFocusListener = listener
    }

    fun setGainedFocusListener(listener: RateItemViewModelListenerOnGainedFocus) {
        this.gainedFocusListener = WeakReference(listener)
    }

    fun setTextChangeListener(listener: RateItemViewModelListenerOnTextChanged) {
        this.textChangeListener = WeakReference(listener)
    }

    fun setTextChangeListener(listener: WeakReference<RateItemViewModelListenerOnTextChanged>?) {
        this.textChangeListener = listener
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is RateItemViewModel) return false

        if (currencyEnum != other.currencyEnum) return false

        return true
    }

    override fun hashCode(): Int {
        return currencyEnum.hashCode()
    }


}

interface RateItemViewModelListenerOnGainedFocus {
    fun onGainedFocus(viewModel: RateItemViewModel)
}

interface RateItemViewModelListenerOnTextChanged {
    fun onTextChanged(text: String)
}