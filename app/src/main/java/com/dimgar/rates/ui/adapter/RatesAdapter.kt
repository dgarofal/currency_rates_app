package com.dimgar.rates.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.dimgar.rates.databinding.RowRateItemBinding
import com.dimgar.rates.ui.adapter.viewmodel.RateItemViewModel
import com.dimgar.rates.ui.adapter.viewmodel.RateItemViewModel.Companion.update
import com.dimgar.rates.ui.adapter.viewmodel.RateItemViewModelDiffUtil
import com.dimgar.rates.ui.adapter.viewmodel.RateItemViewModelListenerOnGainedFocus
import com.dimgar.rates.ui.adapter.viewmodel.RateItemViewModelListenerOnTextChanged
import java.lang.ref.WeakReference

class RateItemViewHolder(
    private val itemBinding: RowRateItemBinding
) : RecyclerView.ViewHolder(itemBinding.root) {

    fun bind(
        viewModel: RateItemViewModel,
        gainedFocusListener: WeakReference<RateItemViewModelListenerOnGainedFocus>,
        textChangeListener: RateItemViewModelListenerOnTextChanged
    ) {
        viewModel.setGainedFocusListener(gainedFocusListener)
        viewModel.setTextChangeListener(textChangeListener)

        itemBinding.viewModel = viewModel
        itemBinding.executePendingBindings()
    }

}

class RatesAdapter(listener: RateItemViewModelListenerOnGainedFocus) :
    RecyclerView.Adapter<RateItemViewHolder>(), RateItemViewModelListenerOnTextChanged {


    private val listener = WeakReference(listener)
    private var layoutInflater: LayoutInflater? = null

    private var items = mutableListOf<RateItemViewModel>()

    fun setItems(newItems: MutableList<RateItemViewModel>) {
        val diffResult = DiffUtil.calculateDiff(
            RateItemViewModelDiffUtil(
                newItems,
                this.items
            )
        )
        this.items = newItems
        diffResult.dispatchUpdatesTo(this)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RateItemViewHolder {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }

        return RateItemViewHolder(RowRateItemBinding.inflate(layoutInflater!!, parent, false))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: RateItemViewHolder, position: Int) {
        holder.bind(items[position], listener, this)
    }

    fun onGainedFocus(viewModel: RateItemViewModel?) {
        viewModel ?: return

        items.filter { it != viewModel }.forEach {
            it.hasFocus = false
        }

        val index = items.indexOf(viewModel)
        changePosition(index, viewModel)
    }

    private fun changePosition(index: Int, viewModel: RateItemViewModel) {
        if (index < 0) {
            return
        }

        items.removeAt(index)
        items.add(0, viewModel)

        try {
            notifyItemMoved(index, 0)
        } catch(e: Exception) {
            //
        }
    }

    override fun onTextChanged(text: String) {
        this.items.update(this.items)
        setItems(this.items)
    }
}