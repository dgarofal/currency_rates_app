package com.dimgar.rates.ui.adapter.viewmodel

import androidx.recyclerview.widget.DiffUtil
import com.dimgar.rates.ui.adapter.viewmodel.RateItemViewModel

class RateItemViewModelDiffUtil(
    private val newList: List<RateItemViewModel>,
    private val oldList: List<RateItemViewModel>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]

        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]

        return oldItem.currencyRateText == newItem.currencyRateText
    }
}