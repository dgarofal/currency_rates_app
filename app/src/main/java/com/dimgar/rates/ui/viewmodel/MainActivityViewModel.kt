package com.dimgar.rates.ui.viewmodel

import android.view.View
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.MutableLiveData
import com.dimgar.rates.core.CurrencyEnum
import com.dimgar.rates.core.ObservableViewModel
import com.dimgar.rates.core.ViewModelState
import com.dimgar.rates.repo.RatesRepository
import com.dimgar.rates.ui.adapter.viewmodel.RateItemViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import com.dimgar.rates.ui.adapter.viewmodel.RateItemViewModel.Companion.update

class MainActivityViewModel : ObservableViewModel() {
    var ratesRepository: RatesRepository? = null

    var items = MutableLiveData<MutableList<RateItemViewModel>>()

    private var compositeDisposable = CompositeDisposable()

    var state: ViewModelState? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.state)
        }
        @Bindable get

    var base: CurrencyEnum? = null
        set(value) {
            field = value
            stopObservingRepo()
            observeRepo()
        }

    @Bindable("state")
    fun getNetworkErrorVisibility() = if (state == ViewModelState.LOCAL) View.VISIBLE else View.GONE

    @Bindable("state")
    fun getProgressBarVisibility() = if (state == ViewModelState.LOADING) View.VISIBLE else View.GONE

    @Bindable("state")
    fun getNoDataErrorVisibility() = if (state == ViewModelState.ERROR) View.VISIBLE else View.GONE

    fun observeRepo() {
        ratesRepository?.run {
            compositeDisposable.add(getCurrencies(base)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    // Keep single point of reference. Update the list here, and assign it to the
                    // adapter on activity level. No transformations should take place after here,
                    // in order for items to maintain their position, after configuration change.
                    items.value = items.value.update(it.items)

                    setAndValidateState(it.state)
                }, {
                    Timber.e(it)
                })
            )
        }
    }

    // check if we can show items, even if repo says there's an error
    private fun setAndValidateState(state: ViewModelState) {
        var mState = state

        if (items.value.isNullOrEmpty() && mState == ViewModelState.LOCAL) {
            mState = ViewModelState.LOADING
        }

        if (items.value?.isNotEmpty() == true && mState == ViewModelState.ERROR) {
            mState = ViewModelState.LOCAL
        }

        this.state = mState
    }

    fun stopObservingRepo() {
        compositeDisposable.clear()
    }

    fun restartRepoObserving() {
        stopObservingRepo()
        observeRepo()
    }

    override fun onCleared() {
        super.onCleared()
        if (!compositeDisposable.isDisposed) {
            stopObservingRepo()
        }
    }
}