package com.dimgar.rates.ui

import android.os.Bundle
import android.os.Handler
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.dimgar.rates.core.ViewModelState
import com.dimgar.rates.databinding.ActivityMainBinding
import com.dimgar.rates.repo.RatesRepository
import com.dimgar.rates.ui.adapter.RatesAdapter
import com.dimgar.rates.ui.adapter.viewmodel.RateItemViewModel
import com.dimgar.rates.ui.adapter.viewmodel.RateItemViewModelListenerOnGainedFocus
import com.dimgar.rates.ui.viewmodel.MainActivityViewModel
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject


class MainActivity : DaggerAppCompatActivity(), RateItemViewModelListenerOnGainedFocus,
    SwipeRefreshLayout.OnRefreshListener {

    @Inject
    lateinit var ratesRepository: RatesRepository

    private val ratesAdapter by lazy { RatesAdapter(this) }

    private var binding: ActivityMainBinding? = null

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(MainActivityViewModel::class.java).apply {
            ratesRepository = this@MainActivity.ratesRepository
        }
    }

    private val observer = Observer<MutableList<RateItemViewModel>> {
        ratesAdapter.setItems(it)
        binding?.activityMainSwipeRefreshLayout?.isRefreshing = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupBinding()
        setupRecycler()
        setupSwipeToRefresh()
    }

    private fun setupBinding() {
        binding = DataBindingUtil.setContentView(this, com.dimgar.rates.R.layout.activity_main)
        binding?.viewModel = viewModel
    }

    private fun setupRecycler() {
        binding?.activityMainRecyclerView?.adapter = ratesAdapter
    }

    private fun setupSwipeToRefresh() {
        binding?.activityMainSwipeRefreshLayout?.setOnRefreshListener(this)
    }

    override fun onStart() {
        super.onStart()

        viewModel.items.observe(this, observer)
        viewModel.observeRepo()
    }

    override fun onStop() {
        super.onStop()
        viewModel.stopObservingRepo()
    }

    override fun onRefresh() {
        viewModel.restartRepoObserving()
    }

    override fun onGainedFocus(viewModel: RateItemViewModel) {
        ratesAdapter.onGainedFocus(viewModel)
        this.viewModel.base = viewModel.currencyEnum
    }
}
