package com.dimgar.rates.db.currency

import androidx.room.*
import com.dimgar.rates.api.RatesItemsResponse
import com.dimgar.rates.api.RatesResponse
import com.dimgar.rates.core.CurrencyEnum
import com.dimgar.rates.core.ViewModelState
import com.dimgar.rates.ui.adapter.viewmodel.RateItemViewModel
import com.dimgar.rates.ui.adapter.viewmodel.RateItemViewModels
import java.math.BigDecimal

@Entity(
    tableName = "currency_table",
    indices = [Index(
        value = ["currency_name"],
        unique = true
    )]
)
data class CurrencyEntity(
    @ColumnInfo(name = "currency_id")
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0L,

    @ColumnInfo(name = "currency_name")
    val currencyEnum: CurrencyEnum,

    @ColumnInfo(name = "currency_rate")
    val currencyRate: String
) {
    @Ignore
    private val converters = Converters()

    @ColumnInfo(name = "currency_rate_big_decimal")
    var currencyRateBigDecimal: BigDecimal = converters.fromStringToDoubleRate(currencyRate)
}

fun List<CurrencyEntity>.toRateItemViewModels(state: ViewModelState): RateItemViewModels =
    RateItemViewModels(
        state,
        this
            .map {
                RateItemViewModel(
                    currencyEnum = it.currencyEnum,
                    apiCurrencyRate = it.currencyRate,
                    isBase = it.currencyRate == RatesResponse.CURRENCY_RATE_ONE,
                    apiCurrencyRateBigDecimal = it.currencyRateBigDecimal
                )
            }
            .sortedByDescending { it.isBase }
    )
