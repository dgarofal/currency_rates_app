package com.dimgar.rates.db.currency

import androidx.room.Dao
import androidx.room.Query
import androidx.room.TypeConverter
import com.dimgar.rates.core.CurrencyEnum
import com.dimgar.rates.db.BaseDao
import io.reactivex.Single
import java.lang.Exception
import java.math.BigDecimal
import java.math.RoundingMode

@Dao
interface CurrencyDao: BaseDao<CurrencyEntity> {
    @Query("SELECT * FROM currency_table")
    fun getAll(): Single<List<CurrencyEntity>>

    @Query("""SELECT currency_name FROM currency_table WHERE currency_rate = "1" LIMIT 1""")
    fun getBase(): Single<CurrencyEnum>

}


class Converters {
    @TypeConverter
    fun fromStringToCurrencyEnum(currencyName: String) = CurrencyEnum.valueOf(currencyName)

    @TypeConverter
    fun toStringFromCurrencyEnum(currency: CurrencyEnum) = currency.name

    @TypeConverter
    fun fromStringToDoubleRate(currencyRate: String): BigDecimal {
        if (currencyRate.isEmpty()) {
            return BigDecimal.ONE
        }

        return try {
            BigDecimal(currencyRate).setScale(30, RoundingMode.HALF_EVEN)

        } catch (e: Exception) {
            BigDecimal.ONE
        }
    }

    @TypeConverter
    fun toStringFromDoubleRate(currencyRate: BigDecimal) = currencyRate.toPlainString()

}