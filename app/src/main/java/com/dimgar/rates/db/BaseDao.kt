package com.dimgar.rates.db

import androidx.room.*
import io.reactivex.Single

interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(obj: T): Single<Long>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(list: List<T>): Single<List<Long>>

    @Update
    fun update(vararg obj: T): Single<Int>

    @Update
    fun updateAll(listOfObj: List<T>): Single<Int>

    @Delete
    fun delete(obj: T): Single<Int>

    @Delete
    fun deleteAll(list: List<T>): Single<Int>
}