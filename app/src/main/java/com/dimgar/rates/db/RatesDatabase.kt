package com.dimgar.rates.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.dimgar.rates.BuildConfig
import com.dimgar.rates.db.currency.Converters
import com.dimgar.rates.db.currency.CurrencyDao
import com.dimgar.rates.db.currency.CurrencyEntity

@Database(
    entities = [
        CurrencyEntity::class
    ],
    version = BuildConfig.VERSION_CODE,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class RatesDatabase : RoomDatabase() {
    abstract fun currencyDao(): CurrencyDao
}